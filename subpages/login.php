<?php


$data = new Input();
$session = new Session();
$token = new Token();
$config = new Config();

if($data->isExist()) {
    if($token->check($data->getData("token"))) {
        $login = $data->getData('username');
        $factory = new PDOFactory(new Config());
        $connection = $factory->createConnection();
        $query = new MusicRepository($connection);
        $query->validateUsername($login);
        if (!$query->rowsAffected()) {
            echo "<p style='margin-left: 19px;'>Taka nazwa użytkownika nie istnieje..</p>";
        } else {
            $result = $query->getResult();
            $hash = new Hash();
            if ($hash->check($data->getData('password'), $result['password'])) {
                $session->start();
                $session->set($config->get('session/session_name'),$result['id']);
            } else {
                echo "<p style='margin-left: 19px;'>Podane haslo nie pasuje do nazwy użytkownika..</p>";
                header( "refresh:2; url=index.php?id=login" );
            }
        }
    }
    else{
        echo "Sesja wygasła... Za 2s rozpoczęcie nowej...";
        header( "refresh:2; url=index.php?id=login" );
    }
}
$user = new User();
if($user->isLoggedIn()){
    header("Location: index.php?id=newsfeed");
    die;
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Zaloguj się</h3>
                    <form class="form-horizontal" role="form" action="index.php?id=login" method="post" data-toggle="validator">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id_username">
                                Login:
                            </label>
                            <div class="col-sm-10">
                                <input id="id_username" pattern="^[a-zA-Z0-9_.-]*$" data-minlength="2" maxlength="20" name="username" type="text" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id_password">
                                Hasło
                            </label>
                            <div class="col-sm-10">
                                <input id="id_password" maxlength="30" name="password" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="token" value='<?php $token->generate();echo $session->get("token"); ?>'>
                                <button type="submit" class="btn btn-success">Zaloguj</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    Nie masz jeszcze konta ? <a href="index.php?id=register">Kliknij tu</a> aby sie zarejestrować.
                </div>
            </div>
        </div>
    </div>

</div>