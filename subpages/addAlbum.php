<?php

$user = new User();
if(!$user->isLoggedIn()){
    header("Location: index.php?id=login");
    die;
}

$input = new Input();
if($_FILES && $input->isExist()){
    $fileLogger = new FileLogger();

    if ($fileLogger->isValid($_FILES['albumLogo'])) {
        $config = new Config();
        $session = new Session();
        $album = new AlbumRepository();
        $album->setUserID($session->get($config->get("session/session_name")));
        $album->setArtist($input->getData("artist"));
        $album->setAlbumTitle($input->getData("albumTitle"));
        $album->setGenre($input->getData("genre"));
        $album->setAlbumLogo("media/".$_FILES['albumLogo']['name']);
        $factory = new PDOFactory($config);
        $connection = $factory->createConnection();
        $query = new MusicRepository($connection);
        $query->addAlbum($album);
        if($query->rowsAffected()){
            $fileLogger->uploadFile($_FILES['albumLogo']);
            echo "<p style='margin-left: 19px;'>Album został dodany !</p>";
            unset($_FILES['album_logo']);
        }
        else{
            echo "<p style='margin-left: 19px;'>Przy dodawaniu albumu wystąpił błąd</p>";
        }

    }

}

?>
<div class="container-fluid">

    <div class="row">

        <div class="col-sm-12 col-md-7">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data" data-toggle="validator">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title">Artysta: </label>
                            <div class="col-sm-10"><input id="song_title" type="text" name="artist" maxlength="40" required></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title">Nazwa Albumu: </label>
                            <div class="col-sm-10"><input id="song_title" type="text" name="albumTitle" maxlength="40" required></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title">Gatunek: </label>
                            <div class="col-sm-10"><input id="song_title" type="text" name="genre" maxlength="15" required></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title">Logo albumu: </label>
                            <div class="col-sm-10"><input type="file" name="albumLogo" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Jak dodawać piosenki ?</h3>
                    <p>Na początek stwórz album, aby to zrobić wypełnij formularz na tej stronie. Następnie możesz go edytować i dodawać muzykę.</p>
                    <h3>Jakie powinno być logo albumu?</h3>
                    <ul>
                        <li>Mieć rozdzielczość domyślną 512x512</li>
                        <li>Być w formacie .JPG, .GIF, lub .PNG</li>
                        <li>Ważyć poniżej limitu 2MB.</li>
                        <li>Sugerowane są obrazki w kształcie kwardratu</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

</div>