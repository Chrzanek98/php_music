<?php
$user = new User();
if(!$user->isLoggedIn()){
    header('location: index.php?id=login');
    die;
}
$input = new Input();

$config = new Config();
$session = new Session();
$factory = new PDOFactory($config);
$connection = $factory->createConnection();
$query = new MusicRepository($connection);
$album = new AlbumRepository();
$album->setUserID($session->get($config->get("session/session_name")));
$query->showUserSongs($album);
$result = $query->getResult();


?>

<div class="container-fluid songs-container">


    <div class="row">

        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tytuł</th>
                                <th>Artysta</th>
                                <th>Piosenka</th>
                                <th>Album</th>
                                <th>Favorite</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($result as $item){ ?>
                                <tr>
                                    <td><?php echo $item['song_title'] ?></td>
                                    <td><?php echo $item['artist'] ?></td>
                                    <td>
                                        <a target="_blank" href="<?php echo $item['audio_file'] ?>">
                                            <button type="button" class="btn btn-success btn-xs">
                                                <span class="glyphicon glyphicon-play"></span>&nbsp; Play
</button>
                                        </a>
                                    </td>
                                    <td>
                                            <img src="<?php echo $item['album_logo'] ?>" class="img-responsive" style="width: 20px; float: left; margin-right: 10px;" />
                                        <form action="index.php?id=albumDetails" method="post">
                                            <button type="submit" name="albumID" value="<?php echo $item['album_id'] ?>" class="btn-link"><?php echo $item['album_title'] ?></button>
                                        </form>
                                    </td>
                                    <td>
                                        <a href="#" class="btn-favorite"><span class="glyphicon glyphicon-star"></span></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

</div>