<?php

$data = new Input();
$session = new Session();
$token = new Token();
$user = new User();
if($user->isLoggedIn()){
    header("Location: index.php?id=newsfeed");
    die;
}
$factory = new PDOFactory(new Config());
$connection = $factory->createConnection();
$query = new MusicRepository($connection);
if($data->isExist()){
    if($token->check($data->getData("token"))) {
        $login = $data->getData('username');
        $query->validateUsername($login);

        if ($query->rowsAffected()) {
            echo "<p style='margin-left: 19px;'>Ta nazwa użytkownika jest już zajęta :(</p>";
        } else {
            $hash = new Hash();
            $user = new UserRepository();
            $user->setFirstName($data->getData('firstName'));
            $user->setLastName($data->getData('lastName'));
            $user->setUsername($login);
            $user->setPassword($hash->make($data->getData('password')));
            $user->setJoined(date('Y-m-d H:i:s'));
            $user->setPermissions(1);
            $query->register($user);
            if ($query->rowsAffected()) {
                echo "<p style='margin-left: 19px;'>Rejestracja użytkownika przebiegła <span style='color:green'>pomyślnie</span></p>";
            } else {
                echo "<p>Coś poszło nie tak :(</p>";
            }
        }
        //$session->destroy();
    }
    else{
        echo "<p style='margin-left: 19px;'>Twoja sesja rejestracji została zakończona..Za 2s rozpocznie się kolejna</p>";
        header( "refresh:2; url=index.php?id=login" );
    }
}
$userCount = $query->databaseReveal();
$albumCount = $query->albumsReveal();
?>
<script>
</script>
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Zarejestruj nowe konto i dołącz do <?php echo count($userCount); ?> użytkowników</h3>
                    <form class="form-horizontal" role="form" action="index.php?id=register" method="post" data-toggle="validator">

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <span class="text-danger small"></span>
                            </div>
                            <label class="control-label col-sm-2" for="song_title"><label for="id_firstName">Imie:</label></label>
                            <div class="col-sm-10"><input id="id_firstName" pattern="^[A-Z][-a-zA-Z]+$" data-minlength="2" maxlength="15" name="firstName" type="text" required/></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <span class="text-danger small"></span>
                            </div>
                            <label class="control-label col-sm-2" for="song_title"><label for="id_lastName">Nazwisko:</label></label>
                            <div class="col-sm-10"><input id="id_lastName" data-minlength="2" pattern="^[A-Z][-a-zA-Z]+$" maxlength="20" name="lastName" type="text" required/></div>
                        </div>

                        <div class="form-group">

                            <label class="control-label col-sm-2" for="song_title"><label for="id_username">Login:</label></label>
                            <div class="col-sm-10"><input id="id_username" pattern="^[a-zA-Z0-9_.-]*$" data-minlength="2" maxlength="20" name="username" type="text" required/></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title"><label for="id_password">Hasło:</label></label>
                            <div class="col-sm-10"><input id="id_password" data-minlength="6" maxlength="10" name="password" type="password" required/>
                                <div class="help-block">Minimum 6 znaków</div>
                            </div>
                            <label class="control-label col-sm-2" for="song_title"><label for="id_passwordConfirm">Powtórz hasło:</label></label>
                            <div class="col-sm-10">
                                <input type="password" id="id_passwordConfirm" data-match="#id_password" data-match-error="Ups.. Hasła nie są takie same" placeholder="Potwierdź hasło..." required oninvalid="this.setCustomValidity('Proszę powtórzyć hasło')"/>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="token" value='<?php $token->generate();echo $session->get("token"); ?>'>
                                <button type="submit" class="btn btn-success">Potwierdź</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    Jesteś już zarejestrowany ? <a href="index.php?id=login">Naciśnij tutaj</a> aby się zalogować.
                </div>
            </div>
        </div>
    </div>

</div>

