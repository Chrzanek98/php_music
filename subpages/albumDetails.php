<?php

$user = new User();

$input = new Input();
if(!$input->isExist()){
    header("location: index.php?id=newsfeed");
    die;
}
$config = new Config();
$session = new Session();
$factory = new PDOFactory($config);
$connection = $factory->createConnection();
$query = new MusicRepository($connection);
$song = new SongRepository();

$album = new AlbumRepository();
$album->setAlbumID($input->getData("albumID"));
$connection = $factory->createConnection();
$query->showAlbumById($album);
$result = $query->getResult();
if(isset($_POST['idToDelete'])){
    $song->setSongID($input->getData("idToDelete"));
    $query->deleteSong($song);
}
if($user->checkAlbumPermissions($result['user_id'])) {
    if ($_FILES) {
        $file = new FileLogger();
        $song->setAlbumId($input->getData("albumID"));
        $song->setSongTitle($input->getData("songName"));
        $song->setAudioFile("media/" . $_FILES['audioFile']['name']);
        $query->addSong($song);
        if ($query->rowsAffected()) {
            $file->uploadFile($_FILES['audioFile']);
            echo "<p style='margin-left: 19px;'>Piosenka została dodana !</p>";
            unset($_FILES['audioFile']);
        } else {
            echo "<p style='margin-left: 19px;'>Błąd przy dodawaniu piosenki</p>";
        }
    }
}
$songId = new SongRepository();
$songId->setSongID($input->getData("albumID"));
$songs = $query->showSongById($songId);
$owner = new UserRepository();
$owner->setUserID($result['user_id']);
$query->searchById($owner);
$albumOwner = $query->getResult();
?>
<script>
    $(document).ready(function () {
        var swap = false;

        $("#addSong").hide();

        $("#add").click(function () {
            if(!swap){
                $('#browseSongs').hide();
                $('#addSong').show();
                swap = true;
            }
        });
        $("#browseBack").click(function () {
            if(swap){
                $('#addSong').hide();
                $('#browseSongs').show();
                swap = false;
            }
        });
    })
</script>
<div class="container-fluid songs-container">

    <div class="row">

        <!-- Left Album Info -->
        <div class="col-sm-4 col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                        <img src="<?php echo $result['album_logo'] ?>" class="img-responsive">
                    <h1><?php echo $result['album_title'] ?> <small><?php echo $result['genre'] ?></small></h1>
                    <h2><?php echo $result['artist'] ?></h2>
                </div>
                <small>Dodano przez: <?php echo $albumOwner['firstName'].' '.$albumOwner['lastName'];?></small>
            </div>
        </div>

        <!-- Right Song Info -->
        <div id="browseSongs">
        <div class="col-sm-8 col-md-9">

            <ul class="nav nav-pills" style="margin-bottom: 10px;">
                <li role="presentation" id="browse"><button type="button" class="btn btn-primary active">Zobacz piosenki</button></li>
              <?php if($user->checkAlbumPermissions($result['user_id'])){ ?><li role="presentation" id="add"><button type="button" class="btn btn-primary">Dodaj nową piosenkę</button></li><?php }?>
            </ul>

            <div class="panel panel-default">
                <div class="panel-body">

                    <h3>All Songs</h3>

                    <table class="table">
                        <thead>
                        <tr>
                            <th>Tytuł</th>
                            <th>Plik muzyczny</th>
                            <th>Ulubione</th>
                            <th>Akcja</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($songs as $song) {
                                ?>
                                <tr>
                                    <td><?php echo $song['song_title']?></td>
                                    <td>
                                        <a target="_blank" href="<?php echo $song['audio_file']?>">
                                            <button type="button" onclick="" class="btn btn-success btn-xs">
                                                <span class="glyphicon glyphicon-play"></span>&nbsp; Graj
                                            </button>
                                        </a>
                                    </td>
                                   <td>
                                        <a href="#" class="btn-favorite"><span
                                                    class="glyphicon glyphicon-star {% if song.is_favorite %}active{% endif %}"></span></a>
                                    </td>
                                    <td>  <?php if($user->checkAlbumPermissions($result['user_id'])){?>
                                            <form method="post" action="index.php?id=albumDetails">
                                            <input type="hidden" name="idToDelete" value="<?php echo $song['id']; ?>"/>
                                            <input type="hidden" name="albumID" value="<?php echo $result['album_id']; ?>"/>
                                            <button type="submit" class="btn btn-danger btn-xs">
                                                <span class="glyphicon glyphicon-remove"></span>&nbsp; Usuń
                                            </button>
                                        </form>
                                        <?php } else{ echo '<button type="submit" class="btn btn-danger btn-xs disabled"><span class="glyphicon glyphicon-remove"></span>&nbsp; Usuń</button>';}?>
                                    </td>
                                </tr>
                                <?php
                            }

                        ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
    <div id="addSong">
        <div class="col-sm-8 col-md-9">

            <ul class="nav nav-pills" style="margin-bottom: 10px;">
                <li role="presentation" id="browseBack"><button type="button" class="btn btn-primary">Zobacz piosenki</button></li>&nbsp
                <li role="presentation" id="addBack"><button type="button" class="btn btn-primary active">Dodaj nową piosenkę</button></li>
            </ul>

            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Dodaj nową piosenkę</h3>
                    <form class="form-horizontal" role="form" action="index.php?id=albumDetails" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="albumID" value="<?php echo $result['album_id']?>">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title">Tytuł: </label>
                            <div class="col-sm-10"><input id="song_title" type="text" name="songName" maxlength="40" required></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="song_title">Plik audio: </label>
                            <div class="col-sm-10"><input type="file" name="audioFile" required></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

                </div>

            </div>
    </div>

    </div>

</div>

