<?php

$user = new User();

if(!$user->isLoggedIn()){
    header("Location: index.php?id=login");
    die;
}
$user->logout();

header("Location: index.php?id=login");
die;