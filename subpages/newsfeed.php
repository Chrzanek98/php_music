<?php

$session = new Session();
$user = new User();
$config = new Config();


$factory = new PDOFactory($config);

$connection = $factory->createConnection();

$query = new MusicRepository($connection);

if(isset($_POST['albumID'])){
    $album = new AlbumRepository();
    $album->setAlbumID($_POST['albumID']);
    $query->deleteAlbum($album);
}

$result = $query->albumsReveal();

foreach ($result as $item){
    echo    "<div class='col-sm-4 col-lg-2'>";
    echo    "<div class='thumbnail'>";
    echo    "<img src='".(file_exists($item['album_logo']) ? $item['album_logo'] : 'static/img/no_image.png')."' class='img-responsive'>";
    echo    "<div class='caption'>";
    echo    "<h2>".$item['album_title']."</h2>";
    echo    "<h4>".$item['artist']."</h4>";
    echo    "<form action='index.php?id=albumDetails' method='post' style='display:inline;'>";
    echo    "<input type='hidden' name='albumID' value='".$item['album_id']."'/>";
    echo    "<button type='submit' class='btn btn-primary btn-sm' role='button'>Szczegóły</button></form>";
    if($item['user_id'] == $session->get($config->get('session/session_name')) || $user->isAdmin()) {
        echo    "<form action='index.php?id=newsfeed' method='post' style='display:inline;padding-left:3px;float:right'>";
        echo "<input type='hidden' name='albumID' value='" . $item['album_id'] . "'/>";
        echo "<button type='submit' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-trash'></span></button></form>";
    }
    echo "</div></div></div>";
}