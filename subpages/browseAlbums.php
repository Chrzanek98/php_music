<?php
$user = new User();
$input = new Input();
if(!$user->isLoggedIn()){
    header('location: index.php?id=login');
    die;
}

if(!$input->isExist()){
    header("location: index.php?id=newsfeed");
    die;
}
$session = new Session();
$config = new Config();


$factory = new PDOFactory($config);

$connection = $factory->createConnection();

$query = new MusicRepository($connection);

if(isset($_POST['searchAlbum'])){
    $album = new AlbumRepository();
    $album->setAlbumTitle($_POST['searchAlbum']);
    $query->searchAlbum($album);
}

$result = $query->getResult();
if($result) {
    foreach ($result as $item) {
        echo "<div class='col-sm-4 col-lg-2'>";
        echo "<div class='thumbnail'>";
        echo "<img src='" . (file_exists($item['album_logo']) ? $item['album_logo'] : 'static/img/no_image.png') . "' class='img-responsive'>";
        echo "<div class='caption'>";
        echo "<h2>" . $item['album_title'] . "</h2>";
        echo "<h4>" . $item['artist'] . "</h4>";
        echo "<form action='index.php?id=albumDetails' method='post' style='display:inline;'>";
        echo "<input type='hidden' name='albumID' value='" . $item['album_id'] . "'/>";
        echo "<button type='submit' class='btn btn-primary btn-sm' role='button'>Szczegóły</button></form>";
        if ($item['user_id'] == $session->get($config->get('session/session_name'))) {
            echo "<form action='index.php?id=newsfeed' method='post' style='display:inline;padding-left:3px;float:right'>";
            echo "<input type='hidden' name='albumID' value='" . $item['album_id'] . "'/>";
            echo "<button type='submit' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-trash'></span></button></form>";
        }
        echo "</div></div></div>";
    }
}
else{
    echo "<h2>Brak wyników dla tego zapytania</h2>";
}