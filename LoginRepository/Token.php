<?php


class Token{

    public function generate(){
        $session = new Session();
        $config = new Config();
        $session->set($config->get('session/token_name'),md5(uniqid()));
    }

    public function check($token){
        $config = new Config();
        $session = new Session();
        $tokenName = $config->get('session/token_name');
        if($session->get($tokenName) && $token === $session->get($tokenName)){
            $session->destroy();
            return true;
        }
        return false;
    }


}