<?php


class Hash{

    public function make($string){
        return password_hash($string,PASSWORD_BCRYPT);
    }

    public function check($string,$password){
        return password_verify($string,$password);
    }
    public function unique(){
        return $this->make(uniqid());
    }
}