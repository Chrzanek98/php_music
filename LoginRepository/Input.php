<?php


class Input{

    public function isExist(){
        switch($_SERVER['REQUEST_METHOD']){
            case 'POST':
                return (!empty($_POST));
            case 'GET':
                return ($_GET['id']) ? false : (!empty($_GET));
            default:
                return false;
        }
    }

    public function getData($name){
        if(isset($_POST[$name])){
            return $_POST[$name];
        }
        elseif(isset($_GET[$name])){
            return $_GET[$name];
        }
        else{
//            throw new Exception("Brak wartosci POST");
        }
    }
}