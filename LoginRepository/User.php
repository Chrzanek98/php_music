<?php


class User{

    private $_isLoggedIn = false;
    private $_sessionName;

    public function __construct(){
        $config = new Config();
        $this->_sessionName = $config->get('session/session_name');
    }

    private function isLogged($user = false){
        $userRepository = new UserRepository();
        $session = new Session();
        $factory = new PDOFactory(new Config());
        $connection = $factory->createConnection();
        $query = new MusicRepository($connection);
        $userRepository->setUserID($session->get($this->_sessionName));
        $query->searchById($userRepository);
        if(!$user){
            if($session->get($this->_sessionName)){
                if($query->rowsAffected()){
                    $this->_isLoggedIn = true;
                }else{
                    //TODO logout
                }
            }
        }
        else{
            return $query->rowsAffected();
        }
        return false;
    }

    public function isLoggedIn(){
        $this->isLogged();
        return $this->_isLoggedIn;
    }

    public function isAdmin(){
        $factory = new PDOFactory(new Config());
        $connection = $factory->createConnection();
        $query = new MusicRepository($connection);
        $userRepository = new UserRepository();
        $session = new Session();
        $userRepository->setUserID($session->get($this->_sessionName));
        $query->searchById($userRepository);
        $result = $query->getResult();

        return $result['permissions'] == 2;
    }

    public function logout(){
        $session = new Session();
        $session->destroy();
    }

    public function checkAlbumPermissions($ownerID){
        $session = new Session();
        return $this->isAdmin() || ($session->get($this->_sessionName) == $ownerID);
    }

}