<?php


class Redirect{
    private $id;
    private $subpages = array(
        'register' => 'subpages/register.php',
        'login' => 'subpages/login.php',
        'newsfeed' => 'subpages/newsfeed.php',
        'logout' => 'subpages/logout.php',
        'addAlbum' =>'subpages/addAlbum.php',
        'userAlbums' => 'subpages/userAlbums.php',
        'albumDetails' => 'subpages/albumDetails.php',
        'userSongs' => 'subpages/userSongs.php',
        'browseAlbums' => 'subpages/browseAlbums.php'
    );
    public function __construct($id){
        $this->id = $id;
    }

    public function moveToSubpage(){
        if(array_key_exists($this->id,$this->subpages)){
           include_once($this->subpages[$this->id]);
        }
        else{
            include_once($this->subpages['newsfeed']);
        }
    }
}