<?php


class Session{

    private $_sessionStarted = false;

    public function start(){
        if($this->_sessionStarted == false){
            session_start();
            $this->_sessionStarted = true;
        }
    }

    public function set($key,$value){
        $_SESSION[$key] = $value;
    }

    public function get($key,$secondKey = false){
        if($secondKey){
            if(isset($_SESSION[$key][$secondKey])){
                return $_SESSION[$key][$secondKey];
            }
        }
        else{
            if(isset($_SESSION[$key])){
                return $_SESSION[$key];
            }
        }
        return false;
    }

    public function display(){
        echo '<pre>';
        print_r($_SESSION);
        echo '</pre>';
    }

    public function destroy(){
        session_unset();
        session_destroy();
    }
}