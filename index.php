<?php

require_once('LoginData/Core/init.php');
require_once('DatabaseRepository/PDOFactory.php');
require_once('DatabaseRepository/MusicRepository.php');
require_once('DatabaseRepository/UserRepository.php');
require_once('DatabaseRepository/GroupRepository.php');
require_once('DatabaseRepository/FileLogger.php');
require_once('DatabaseRepository/AlbumRepository.php');
require_once('DatabaseRepository/SongRepository.php');

$user = new User();
if($user->isLoggedIn()) {
    if($user->isAdmin()){
        require_once("templates/base_admin.html");
    }
    else {
        require_once("templates/base.html");
    }
}
else{
    require_once("templates/base_visitor.html");
}

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $redirect = new Redirect($id);
    $redirect->moveToSubpage();
}
else{
    $routeToHomepage = new Redirect('newsfeed');
    $routeToHomepage->moveToSubpage();
}

