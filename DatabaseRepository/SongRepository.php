<?php

class SongRepository{

    private $songID;
    private $album_id;
    private $song_title;
    private $audio_file;

    /**
     * @return mixed
     */
    public function getSongID()
    {
        return $this->songID;
    }

    /**
     * @param mixed $songID
     */
    public function setSongID($songID)
    {
        $this->songID = $songID;
    }

    /**
     * @return mixed
     */
    public function getAlbumId()
    {
        return $this->album_id;
    }

    /**
     * @param mixed $album_id
     */
    public function setAlbumId($album_id)
    {
        $this->album_id = $album_id;
    }

    /**
     * @return mixed
     */
    public function getSongTitle()
    {
        return $this->song_title;
    }

    /**
     * @param mixed $song_title
     */
    public function setSongTitle($song_title)
    {
        $this->song_title = $song_title;
    }

    /**
     * @return mixed
     */
    public function getAudioFile()
    {
        return $this->audio_file;
    }

    /**
     * @param mixed $audio_file
     */
    public function setAudioFile($audio_file)
    {
        $this->audio_file = $audio_file;
    }

}