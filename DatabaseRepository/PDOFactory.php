<?php


class PDOFactory{

    private $config;

    public function __construct(Config $config){
        $this->config = $config;
    }

    public function createConnection(){
        try {
            return new PDO('mysql:host=' . $this->config->get('mysql/host') . ';dbname=' . $this->config->get('mysql/db'), $this->config->get('mysql/username'), $this->config->get('mysql/password'));
        }
        catch (Exception $e){
            die("Message: ".$e->getMessage());
        }
    }

}
