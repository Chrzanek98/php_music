<?php


class FileLogger{
    private $_supportedImages = ['image/png','image/gif','image/jpeg'];
    private $_audioSupported= ['audio/mpeg','audio/wav'];

    private function isImage($file){
        return in_array($file['type'], $this->_supportedImages);
    }

    private function isAudio($file){
        return in_array($file['type'], $this->_audioSupported);
    }

    private function isSizeSupported($file){
        return $file['size'] < 10485760;
    }


    public function isValid($file){
        return is_array($file) && $this->isSizeSupported($file) && $this->isImage($file);
    }

    public function isAudioValid($file){
        return is_array($file) && $this->isSizeSupported($file) && $this->isAudio($file);
    }

    public function uploadFile($file){
        move_uploaded_file($file['tmp_name'],'media/'.$file['name']);
    }

}