<?php


class MusicRepository{
    private $_connection;
    private $_result;
    private $_rowsAffected;

    public function __construct($connection){
        $this->_connection = $connection;
        $this->_connection->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
    }

    public function databaseReveal(){
        $this->_result = $this->_connection->query("SELECT * FROM users");
        return $this->_result->fetchAll();
    }

    public function searchById(UserRepository $user){
        $sth = $this->_connection->prepare("SELECT * FROM users WHERE id = :id");
        $sth->bindParam(':id',$user->getUserID());
        $sth->execute();
        $this->_result = $sth->fetch();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function findGroupById(GroupRepository $group){
        $sth = $this->_connection->prepare("SELECT * FROM groups WHERE id = :id");
        $sth->bindParam(':id',$group->getGroupID());
        $sth->execute();
        $this->_result = $sth->fetch();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function deleteRow(UserRepository $user){
        $sth = $this->_connection->prepare("DELETE FROM users WHERE id = :id");
        $sth->bindParam(':id',$user->getUserID());
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function register(UserRepository $user){
        $sth = $this->_connection->prepare("INSERT INTO users (username,password,firstName,lastName,joined,permissions) VALUES (:username,:password,:firstName,:lastName,:joined,:permissions) ");
        $sth->bindParam(':username',$user->getUsername());
        $sth->bindParam(':password',$user->getPassword());
        $sth->bindParam(':firstName',$user->getFirstName());
        $sth->bindParam(':lastName',$user->getLastName());
        $sth->bindParam(':joined',$user->getJoined());
        $sth->bindParam(':permissions',$user->getPermissions());
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function validateUsername($username){
        $sth = $this->_connection->prepare('SELECT * FROM users WHERE username like :username');
        $sth->bindParam(':username',$username);
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
        $this->_result = $sth->fetch();
    }

    public function addAlbum(AlbumRepository $album){
        $sth = $this->_connection->prepare('INSERT INTO album (user_id,artist,album_title,genre,album_logo) VALUES (:ui,:ar,:ati,:g,:al)');
        $sth->bindParam(':ui',$album->getUserID());
        $sth->bindParam(':ar',$album->getArtist());
        $sth->bindParam(':ati',$album->getAlbumTitle());
        $sth->bindParam(':g',$album->getGenre());
        $sth->bindParam(':al',$album->getAlbumLogo());
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function albumsReveal(){
        $this->_result = $this->_connection->query("SELECT * FROM album");
        return $this->_result->fetchAll();
    }

    public function userAlbums(AlbumRepository $album){
        $sth = $this->_connection->prepare("SELECT * FROM album where user_id = :id");
        $sth->bindParam(':id',$album->getUserID());
        $sth->execute();
        $this->_result = $sth->fetchAll();
    }

    public function showAlbumById(AlbumRepository $album){
        $sth = $this->_connection->prepare("SELECT * FROM album where album_id = :id");
        $sth->bindParam(':id',$album->getAlbumID());
        $sth->execute();
        $this->_result = $sth->fetch();
    }

    public function addSong(SongRepository $song){
        $sth = $this->_connection->prepare("INSERT INTO song (album_id,song_title,audio_file) VALUES (:ai,:st,:af)");
        $sth->bindParam(':ai',$song->getAlbumId());
        $sth->bindParam(':st',$song->getSongTitle());
        $sth->bindParam(':af',$song->getAudioFile());
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function songsReveal(){
        $this->_result = $this->_connection->query("SELECT * FROM song");
        return $this->_result->fetchAll();
    }

    public function showSongById(SongRepository $song){
        $sth = $this->_connection->prepare("SELECT * FROM song where album_id = :id");
        $sth->bindParam(':id',$song->getSongID());
        $sth->execute();
        return $this->_result = $sth->fetchAll();
    }

    public function deleteSong(SongRepository $song){
        $sth = $this->_connection->prepare("DELETE FROM song WHERE id = :id");
        $sth->bindParam(':id',$song->getSongID());
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function deleteAlbum(AlbumRepository $album){
        $sth = $this->_connection->prepare("DELETE FROM album WHERE album_id = :id");
        $sth->bindParam(':id',$album->getAlbumID());
        $sth->execute();
        $this->_rowsAffected = $sth->rowCount();
    }

    public function showUserSongs(AlbumRepository $album){
        $sth = $this->_connection->prepare("select * from song inner join album on album.album_id = song.album_id WHERE user_id = :id");
        $sth->bindParam(':id',$album->getUserID());
        $sth->execute();
        $this->_result = $sth->fetchAll();
    }

    public function searchAlbum(AlbumRepository $album){
        $val = "%".$album->getAlbumTitle()."%";
        $sth = $this->_connection->prepare("SELECT * FROM album where album_title like :namea or artist like :namea or genre like :namea");
        $sth->bindParam(':namea',$val);
        $sth->execute();
        $this->_result = $sth->fetchAll();
    }

    public function getResult(){
        return $this->_result;
    }

    public function rowsAffected(){
        return $this->_rowsAffected;
    }
}

