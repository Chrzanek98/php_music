<?php

class UserRepository{
    private $userID;
    private $username;
    private $firstName;
    private $lastName;
    private $salt;
    private $joined;
    private $permissions;
    private $password;

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }


    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getUserID(){
        return $this->userID;
    }

    /**
     * @param mixed $userID
     */
    public function setUserID($userID){
        $this->userID = $userID;
    }

    /**
     * @return mixed
     */
    public function getUsername(){
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username){
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getFirstName(){
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName){
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getSalt(){
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt){
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getJoined(){
        return $this->joined;
    }

    /**
     * @param mixed $joined
     */
    public function setJoined($joined){
        $this->joined = $joined;
    }

    /**
     * @return mixed
     */
    public function getPermissions(){
        return $this->permissions;
    }

    /**
     * @param mixed $permissions
     */
    public function setPermissions($permissions){
        $this->permissions = $permissions;
    }
}