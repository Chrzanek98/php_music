<?php

class AlbumRepository{
    private $userID;
    private $albumID;
    private $artist;
    private $albumTitle;
    private $genre;
    private $album_logo;
    private $isFavourite;

    /**
     * @return mixed
     */
    public function getAlbumID()
    {
        return $this->albumID;
    }

    /**
     * @param mixed $albumID
     */
    public function setAlbumID($albumID)
    {
        $this->albumID = $albumID;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param mixed $userID
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getAlbumTitle()
    {
        return $this->albumTitle;
    }

    /**
     * @param mixed $albumTitle
     */
    public function setAlbumTitle($albumTitle)
    {
        $this->albumTitle = $albumTitle;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return mixed
     */
    public function getAlbumLogo()
    {
        return $this->album_logo;
    }

    /**
     * @param mixed $album_logo
     */
    public function setAlbumLogo($album_logo)
    {
        $this->album_logo = $album_logo;
    }

    /**
     * @return mixed
     */
    public function getIsFavourite()
    {
        return $this->isFavourite;
    }

    /**
     * @param mixed $isFavourite
     */
    public function setIsFavourite($isFavourite)
    {
        $this->isFavourite = $isFavourite;
    }

}